import logging                  # Builtin
import sqlite3
import pytest                   # 3rd party
from compute import compute     # Local


@pytest.fixture
def dataset():
    conn = sqlite3.connect(':memory:')
    cursor = conn.cursor()
    records = [('chemistry', 17), ('biology', 25)]
    cursor.execute('CREATE TABLE optimal (domain text, result real)')
    cursor.executemany('INSERT INTO optimal VALUES (?, ?)', records)
    conn.commit()
    logging.debug('Created %s', conn)
    yield conn
    conn.close()
    logging.debug('Closed %s', conn)


def test_dataset_converges(dataset):
    assert compute(dataset) == 42
