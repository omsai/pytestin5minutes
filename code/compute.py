def compute(dataset):
    cursor = dataset.cursor()
    cursor.execute('SELECT SUM(result) FROM optimal')
    return cursor.fetchone()[0]
