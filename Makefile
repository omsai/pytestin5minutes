version := 2.3
focus := https://github.com/elauksap/focus-beamertheme/archive/v$(version).tar.gz
tarball := $(notdir $(focus))
tardir := focus-beamertheme-$(version)
theme_infixes := color inner outer
theme_font_file := beamerfontthemefocus.sty
theme_other_files := $(addsuffix themefocus.sty,$(addprefix beamer,$(theme_infixes)))
theme_files := beamerthemefocus.sty $(theme_other_files)

.PHONY : default
default : pytestin5minutes.pdf pytestin5minutes-handout.pdf

%.pdf : %.tex img theme
	latexmk -pdf $<

# .INTERMEDIATE : %-handout.tex
%-handout.tex : %.tex
	sed -E 's#(documentclass\[)#\1handout,#' < $< > $@

.PHONY : img
img :
	$(MAKE) -C $@/

.PHONY : theme
theme : $(theme_files) $(theme_font_file)

$(theme_font_file) : $(tarball)
	tar --strip-components=1 -xf $< $(tardir)/$@
# Remove ugly capital case!
	patch < $@.patch

$(theme_files) : $(tarball)
	tar --strip-components=1 -xf $< $(tardir)/$@
# Fix age of tarball to not be newer than file contents.
	touch -r $@ $<

$(tarball) :
	wget $(focus) -O $@

.PHONY : clean clean-sty
clean :
	git check-ignore * | grep -v -e .sty -e .tar.gz | xargs rm -f

clean-sty :
	rm -f *.sty
