\documentclass[aspectratio=169,color=usenames,dvipsnames]{beamer}
\usepackage{listings}
\usepackage{lstautogobble}      % Remove leading indentation in lsitings
\usepackage[backend=biber,style=numeric-comp,sorting=none]{biblatex}
\addbibresource{references.bib}

% Highlight lines
%
% lstlinebgrd isn't compatible with texlive2018, so use workaround
% https://tex.stackexchange.com/a/451538
\makeatletter
\let\old@lstKV@SwitchCases\lstKV@SwitchCases
\def\lstKV@SwitchCases#1#2#3{}
\makeatother
\usepackage{lstlinebgrd}
\makeatletter
\let\lstKV@SwitchCases\old@lstKV@SwitchCases

\lst@Key{numbers}{none}{%
    \def\lst@PlaceNumber{\lst@linebgrd}%
    \lstKV@SwitchCases{#1}%
    {none:\\%
     left:\def\lst@PlaceNumber{\llap{\normalfont
                \lst@numberstyle{\thelstnumber}\kern\lst@numbersep}\lst@linebgrd}\\%
     right:\def\lst@PlaceNumber{\rlap{\normalfont
                \kern\linewidth \kern\lst@numbersep
                \lst@numberstyle{\thelstnumber}}\lst@linebgrd}%
    }{\PackageError{Listings}{Numbers #1 unknown}\@ehc}}
\makeatother

\usetheme[numbering=progressbar]{focus}
\definecolor{background}{RGB}{255, 255, 255} % Match with pytest logo.
\setbeamerfont{footnote}{size=\tiny}
\lstset{
  language=python,
  autogobble,
  showstringspaces=false,
  upquote=true,
  basicstyle=\small\ttfamily,
  % For color names, see
  % https://en.wikibooks.org/wiki/LaTeX/Colors#The_68_standard_colors_known_to_dvips
  commentstyle=\color{BrickRed},
  keywordstyle=\color{Purple},
  stringstyle=\color{BrickRed},
  classoffset=1,
  morekeywords={True, False, @pytest, fixture},
  keywordstyle=\color{ForestGreen},
  classoffset=0,
  morekeywords={assert, yield},
}

\graphicspath{{img/}}
\title{Unit testing in 5 minutes}
\titlegraphic{
  \begin{picture}(0,0)
    \put(-100,-75){\includegraphics[scale=2]{pytest1}}
  \end{picture}}                % chktex 31
\subtitle{Greater Hartford Python meetup}
\author{Pariksheet Nanda}
\institute{University of Connecticut}
\date{Wednesday, May 29, 2019}

\begin{document}

\begin{frame}
  \titlepage{}
\end{frame}

\begin{frame}[fragile]{Unit tests preserve manual work}
  \begin{itemize}[<+->]
  \item You already write tests all the time!
    \begin{itemize}[<+->]
    \item Interactive tests
      \begin{lstlisting}
        >>> compute(dataset) == 42
        True
      \end{lstlisting}
    \item Print statements
      \begin{lstlisting}
        result = compute(dataset)
        print(result)
        # 42
      \end{lstlisting}
    \end{itemize}
  \item Saving that test as a function recycles, reuses and reduces
    wasted validation!
    \begin{lstlisting}
      def test_dataset_converges(dataset):
          assert compute(dataset) == 42
    \end{lstlisting}
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Test fixtures replace real objects}
  \begin{lstlisting}[%
    linebackgroundcolor={%
      \ifnumequal{\value{lstnumber}}{9}{\color{yellow!30}}{\color{white}}%
    }]
      @pytest.fixture
      def dataset():
          conn = sqlite3.connect(':memory:')
          cursor = conn.cursor()
          records = [('chemistry', 17), ('biology', 25)]
          cursor.execute('CREATE TABLE optimal (domain text, result real)')
          cursor.executemany('INSERT INTO optimal VALUES (?, ?)', records)
          conn.commit()
          yield conn
          conn.close()


      def test_dataset_converges(dataset):
          assert compute(dataset) == 42
  \end{lstlisting}
\end{frame}

\begin{frame}[fragile]{Unit test frameworks create sandboxes for you}
  \begin{itemize}[<+->]
  \item To reproducibly test code, you need \alert{fixtures}. Fixtures
    pretend to be:
    \begin{enumerate}
    \item<.-> Databases
    \item<+-> Files and directories
    \item<.-> E-mail SMTP servers
    \item<.-> Datetime
    \item<.-> \ldots
    \end{enumerate}
  \item In \texttt{pytest}, fixtures like
    \lstinline[emph=dataset,emphstyle=\underbar]{dataset} are passed
    as arguments to \texttt{test\_*} functions:
    \begin{lstlisting}[emph=dataset,emphstyle=\underbar]
      def test_dataset_converges(dataset):
          assert compute(dataset) == 42
    \end{lstlisting}
  \item<.-> For each fixture, \alert{setup} and \alert{teardown}
    before and after \lstinline{yield}
  \item Therefore fixtures provide a sandbox replacement for your code
    to interact with
  \end{itemize}
\end{frame}

\begin{frame}[fragile]{Many advantages of unit tests}
  \begin{enumerate}[<+->]
  \item Improves code design
  \item Documents complex code
  \item Faster to fix bugs by adding a test that reproduces the fault
  \item Badge of honor: quality and transparency
    \begin{tabular}{cccc}
    \includegraphics[scale=0.5]{cov15} &
    \includegraphics[scale=0.5]{cov45} &
    \includegraphics[scale=0.5]{cov80} &
    \includegraphics[scale=0.5]{cov97}
    \end{tabular}
    % This coverage.py output is straight from
    % https://coverage.readthedocs.io/
    \begin{uncoverenv}<+->
      \begin{lstlisting}[language=]
# coverage report -m
Name                 Stmts   Miss  Cover   Missing
----------------------------------------------------
my_program.py           20      4    80%   33-35, 39
my_other_module.py      56      6    89%   17-23
----------------------------------------------------
TOTAL                   76     10    87%
      \end{lstlisting}
    \end{uncoverenv}
  \end{enumerate}
\end{frame}

\begin{frame}[fragile]{pytest\footnote{The project used to be called  \texttt{py.test}} is simple but powerful from its plugins}
  \begin{itemize}
  \item Many plugins to add fixtures
  \item \ldots including plugins to interact with other python programs:
    \begin{itemize}
    \item \texttt{pytest-coverage}
    \item \texttt{pytest-pylint}
    \end{itemize}
  \end{itemize}
  \begin{overlayarea}{\linewidth}{7cm}
    \begin{onlyenv}<2->
      \begin{lstlisting}[language=]
============================= FAILURES =============================
_______________________ [pylint] compute.py ________________________
C:  1, 0: Missing module docstring (missing-docstring)
C:  1, 0: Missing function docstring (missing-docstring)
_____________________ [pylint] test_compute.py _____________________
C:  1, 0: Missing module docstring (missing-docstring)
C:  8, 0: Missing function docstring (missing-docstring)
W: 21,27: Redefining name 'dataset' from outer scope (line 8) (redefined-outer-name)
C: 21, 0: Missing function docstring (missing-docstring)
======== 2 failed, 1 passed, 2989 warnings in 1.39 seconds =========
    \end{lstlisting}
  \end{onlyenv}
  \end{overlayarea}
\end{frame}

\begin{frame}{Special thanks}
  I originally learned unit testing 3+ years
  ago\footnote{\url{https://www.meetup.com/greaterhartfordpython/events/224608450/}}
  at this meetup from Jonathan Herzog of MIT Lincoln Labs\\~\\
  Reviewers:
  \begin{itemize}
  \item Gogarten lab
  \end{itemize}
\end{frame}

\appendix
\begin{frame}{Resources to use tests in your next Python project}
  \begin{enumerate}
  \item Unofficial Software Carpentry lesson:
    \url{https://katyhuff.github.io/python-testing/}
  \item Book of adding tests to complex code:
    \fullcite{feathers_working_2005}
  \item Other unit testing frameworks:
    \begin{itemize}
    \item \texttt{unittest} module bundled with Python is usable, but
      not as powerful
    \item \texttt{nose2} is \texttt{unittest} with plugins, but
      recommends starting out with \texttt{pytest}
  \end{itemize}
  \item Examples
    \begin{itemize}
    \item Hundreds of tests for Conway's game of life using
      \texttt{pytest.parametrize}:
      \url{https://gitlab.com/omsai/gdcr2018}
    \item Many types of tests:
      \url{https://github.uconn.edu/HPC/gpfsclean}
    \end{itemize}
  \item \texttt{pytest} homepage: \url{https://pytest.org}
  \item This presentation: \url{https://omsai.gitlab.io/pytestin5minutes}
  \end{enumerate}
\end{frame}

\end{document}
